import { CalendarEvent } from 'calendar-utils';

export interface CustomData extends CalendarEvent {
    city?: string
  }