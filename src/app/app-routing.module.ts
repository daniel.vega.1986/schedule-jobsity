import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScheduleComponent } from './schedule/schedule.component';


const routes: Routes = [
  { 
    path:  '', 
    component: ScheduleComponent
  },
  { 
    path:  '', 
    redirectTo: '', 
    pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
