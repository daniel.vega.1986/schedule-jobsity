import { Component, ChangeDetectionStrategy, OnInit, ViewChild, TemplateRef, Input } from '@angular/core';
import { CustomData } from 'src/entities/entitiesSchedule';
import { AddEventService } from './addEvent.service';
import { EventColor, CalendarEvent } from 'calendar-utils';
import { CalendarEventAction } from 'angular-calendar';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'addEvent',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: 'addEvent.template.html'
})
export class AddEventComponent implements OnInit {
    @Input('modalEventNew') modalEventNew: NgbModal;
    @Input('events') events: CustomData[];
    @ViewChild('formEvent', {static: true}) ngForm: NgForm;
    @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;
    newEvent: CustomData;
    newColor: EventColor;
    modalData: {
        action: string;
        event: CalendarEvent;
    };

    constructor(private addService: AddEventService, private modal: NgbModal) { }
    ngOnInit() {
        this.newColor = {
            primary: '',
            secondary: ''
        };
        this.newEvent = {
            title: '',
            //actions: this.actions,
            start: undefined,
            end: undefined,
            color: this.newColor,
            city: undefined
        }
    }
    handleEvent(action: string, event: CalendarEvent): void {
        if (action === 'Edited') {
            this.newEvent = event;
            this.modalData = { event, action };
            this.modal.open(this.modalContent, { size: 'lg' });
        }
    }

    addEvent() {
        this.addService.sendData(this.newEvent);
        this.modalEventNew.dismissAll();
    }
    close() {
        this.modalEventNew.dismissAll();
    }
}