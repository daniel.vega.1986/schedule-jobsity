import { TestBed, ComponentFixture } from '@angular/core/testing';
import { AddEventComponent } from './addEvent.component';
import { FormsModule } from '@angular/forms';
import { Subject } from 'rxjs';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('Component: AddEvent', () => {
    let fixture: ComponentFixture<AddEventComponent>;
    let component: AddEventComponent;
    let debugElement: DebugElement;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FormsModule],
        declarations: [AddEventComponent],
      }).compileComponents();
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(AddEventComponent);
      component = fixture.componentInstance;
      debugElement = fixture.debugElement;
      
    });
    it('should bind the input to the correct property', () => {
        // first round of change detection
        fixture.detectChanges();
        // get ahold of the input
        let input = debugElement.query(By.css('#title'));
        let inputElement = input.nativeElement;
      });
});