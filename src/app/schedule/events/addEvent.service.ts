import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';
import { CustomData } from 'src/entities/entitiesSchedule';

@Injectable()
export class AddEventService {
    data: CustomData;
    private eventSource = new BehaviorSubject<CustomData>(this.data);
    item = this.eventSource.asObservable();
    sendData(newEvent: CustomData) {
        this.eventSource.next(newEvent);
    }
}