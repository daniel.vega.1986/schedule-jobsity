import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
  OnInit,
  OnDestroy,
  OnChanges
} from '@angular/core';
import {
  isSameDay,
  isSameMonth
} from 'date-fns';
import { Subject, Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView
} from 'angular-calendar';
import { EventColor } from 'calendar-utils';
import { CustomData } from 'src/entities/entitiesSchedule';
import { AddEventService } from './events/addEvent.service';
import { ScheduleService } from './schedule.service';


@Component({
  selector: 'schedule',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['schedule.styles.css'],
  templateUrl: 'schedule.template.html'
})
export class ScheduleComponent implements OnDestroy, OnInit {
  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;
  @ViewChild('modalEventNew', { static: true }) modalEventNew: TemplateRef<any>;
  @ViewChild('modalContentSee', { static: true }) modalContentSee: TemplateRef<any>;

  subscription: Subscription;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CustomData }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();
  events: CustomData[] = [];

  activeDayIsOpen: boolean = true;

  newEvent: CustomData;
  newColor: EventColor;

  forecast: string;

  constructor(private modal: NgbModal, private addService: AddEventService, private scheduleService: ScheduleService) { }

  ngOnInit() {
    this.getData();
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getData() {
    this.subscription = this.addService.item.subscribe(
      result => {
        if (result) {
          result.actions = this.actions;
          this.events = [...this.events, result]
        }
      }
    );
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CustomData): void {
    switch (action) {
      case 'Clicked':
        this.scheduleService.getWeatherByTimeAndCordinates(event.city, event.start).subscribe(
          result => {
            this.forecast = result.currently.summary;
            this.newEvent = event;
            this.modalData = { event, action };
            this.modal.open(this.modalContentSee, { size: 'lg' });
          }
        );
        break;
      case 'Edited':
        this.newEvent = event;
        this.modalData = { event, action };
        this.modal.open(this.modalContent, { size: 'lg' });
        break;
    }
  }

  editEvent() {
    this.modal.dismissAll();
  }

  addNewEvent() {
    this.modal.open(this.modalEventNew, { size: 'lg' });
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter(event => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }
}
