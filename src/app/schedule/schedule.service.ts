import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiForecast } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {
  private APIKEY: string;

  cities = [
    {
      city: 'Quito',
      lat: -0.1859053,
      long: -78.7114315
    },
    {
      city: 'Guayaquil',
      lat: -2.1520378,
      long: -80.1202942
    },
    {
      city: 'Cuenca',
      lat: -2.892183,
      long: -79.0244856
    }
  ]


  constructor(private http: HttpClient) {}

  getWeatherByTimeAndCordinates(city: string, date: Date): Observable<any> {
    let cityChoose = this.cities.filter(a => a.city === city)[0];
    return this.http.get('https://cors-anywhere.herokuapp.com/' + apiForecast.URL + apiForecast.APIKEY + '/' + cityChoose.lat + ',' + cityChoose.long + ',' + date.getTime() / 1000);
  }
}
